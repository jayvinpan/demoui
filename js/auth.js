define(["lib", "const","bootstrap-dialog","jquery.validate"], function(LIB, CONST,Dialog) {
	var _login_window = "_login_ui";
	var _login_button = "login_btn";
	var loginDialog = new Dialog({title: '登陆',message:"111"});
	var MODEL = {
		init: function() {
			$("#login_form").validate({
				errorElement : 'span',
				errorClass : 'help-block',
				focusInvalid : true,
				onkeyup : false,
				rules : {
					username : {
						required : true,
						maxlength: 50
					},
					password : {
						required : true
					}
				},
				messages : {
					username : {
						required : "请输入登陆名。",
						maxlength:$.format("登陆名不能大于{0}个字符")
					},
					password : {
						required : "请输入密码。"
					}
				},
				showErrors:function(errorMap,errorList) {
					this.defaultShowErrors();
				},
				highlight : function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},
				errorPlacement : function(error, element) {
					error.insertAfter(element.closest('.input-group'));
				},
				submitHandler : function(form) {

				}
			});
			$('#login_form input').keydown(function(e) {
				if (e.which == 13) {
					if ($('#login_form').validate().form()) {
					}
					return false;
				}
			});
			this.base();
			//this.verify();
		},
		verify:function(){
			//http://www.oschina.net/question/2563496_2154416
			var _this = this;
			var maxTime = 1*1 * 30; // seconds
			var time = maxTime;
			$('body').on('keydown mousemove mousedown', function(e){
			    time = maxTime; // reset
			    console.log("has move reset time~~~~");
			});
			var intervalId = setInterval(function(){
			    time--;
			    if(time <= 0) {
			    	_this.auth();
			        clearInterval(intervalId);
			    }
			}, 1000);
		},
		login:function(){
			var param = {"random":LIB.random()};
			param = $.extend(param,{"name":$("#loginname").val()});
			param = $.extend(param,{"password":$("#password").val()});
			$.ajax({
				type: "POST",
				url: CONST.ROUTE.LOGIN_ACTION,
				data: JSON.stringify(param), 
				async: false,
				dataType: "json",
				contentType: 'application/json',      
				success: function(data) {
					_menus = data;
					console.log("URI  [" + CONST.ROUTE.LOGIN_ACTION + " ]" + data + "  " + document.location);
					if(LIB.isObject(data) && data.code === CONST.STATUS.SUCCESS){
	
						window.location.replace("main.shtml");
					} else {
						$("#info").html(data.message);
					}
				}
			});
		},
		windowLoad:function(){
			t = this;
			$("#login_btn").click(function(){
				//t.login();
			});
		},
		base:function(){
			_this = this;
			$.ajaxSetup({
				//contentType: 'application/json;charset=utf-8', 
				//contentType: "application/x-www-form-urlencoded;charset=utf-8",
				cache: false,
				complete: function(XMLHttpRequest, textStatus) {
					try {
						var data = $.parseJSON(XMLHttpRequest.responseText);
						console.log("ajax filter data:[" + data + "    " + LIB.isObject(data) + "]");
						if(LIB.isObject(data) && data.status === CONST.STATUS.SESSION_TIME_OUT){
							_this.lockScreen();
						}
					} catch(e) {};
					
				}
			});
			console.log("ajax filter init ok! ");
		},
		auth:function(){
			var _this = this;
			$.ajax({
				type: "get",
				url: CONST.ROUTE.AUTH_ACTION,
				async: false,
				dataType: "json",
				success: function(data) {
					_menus = data;
					console.log("URI  [" + CONST.ROUTE.AUTH_ACTION + " ]" + data);
					if(LIB.isObject(data) && data.status === CONST.STATUS.SUCCESS){
						console.log("auth verify ok.");
					} else {
						_this.lockScreen();
					}
				}
			});
		},
		lockScreen:function(){
			loginDialog.open();
			Dialog.show({
				draggable: true,
				animate: false,
				closable:false,
				autodestroy:false,
				message: function(dialog) {
					var $message = $('<div></div>').load('page/login.html');//alert($message.html());
					return $message;
				},
				nl2br:false,
				buttons: [{
							id: 'button-c',
							label: '(C) Button C',
							hotkey: 67,
							action: function(){
								alert('This is Button C but you won\'t see me dance.');
							}
						},{
							label: 'Close this dialog.',
							action: function(dialogRef){
								dialogRef.close();
							}
						},]
			});
		}
	};
	MODEL.init();
	return MODEL;
});