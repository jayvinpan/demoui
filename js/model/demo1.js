//http://nakupanda.github.io/bootstrap3-dialog/
define(["bootstrap-table","lib","bootstrap-dialog"], function(table,LIB,Dialog) {
	var MODEL ={
		init:function(){
			Dialog.show({
				title: 'Say-hello dialog',
				message: 'Hi Apple!'
			});
		}
	};
	return MODEL;
});