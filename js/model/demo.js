define(["lib","bootstrap-dialog","bootstrap-table","bootstrap-table-zh-CN","jquery.validate"], function(LIB,Dialog,k,s) {
	var MODEL ={
		init:function(){
			console.log("init demo-----------------1");
			this.list();
			console.log("init demo-----------------2");
			$("#test_form").validate({});
		},
		list:function(){
			_this = this;//LIB.getConst().ROUTE.APPS_GET_ACTION
			//http://bootstrap-table.wenzhixin.net.cn/documentation/
			$("#table3").bootstrapTable({
				url:LIB.getConst().ROUTE.DATA_ACTION,
				pagination: true,
				//height: 450,
				striped: true,
				sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1,                       //初始化加载第一页，默认第一页
                pageSize: 10,                       //每页的记录行数（*）
                pageList: [1, 2, 3, 4],        //可供选择的每页的行数（*）
                uniqueId: "id",
                columns: [
                	{field: "id",title: "序号",formatter:function(value,row,index){  
		                	return index; 
		            }},
                	{field: "title",title: "标题"},
                	{field: "film",title: "电影"},
                	{field: "os",title:"操作",
                		formatter:function(value,row,index){  
		                	return ' <a href="#" class="modify" index="'+ index +'" >查看</a>'; 
		                }                		
                	}
                ],
                search:false,
                onClickCell:function(field,value,row){
		        	console.log("click cell " + field + value + row);;
		        	if(field=="id"){
						Dialog.show({
							title: 'Say-hello dialog1',
							nl2br:false,
							autodestroy:false,
							message: function(dialog) {
								var $message = $('#demo-myedit').html();
								//var pageToLoad = dialog.getData('pageToLoad');
								//$message.load(pageToLoad);
						
								return $message;
							}
						});
		        	}else {
						Dialog.show({
							title: 'Say-hello dialog2',
							message: function(dialog) {
								var $message = $('#demo-myedit2').html();
								//var pageToLoad = dialog.getData('pageToLoad');
								//$message.load(pageToLoad);
								return $message;
							},
							nl2br:false,
							autodestroy:false,
							closable:false,
							draggable: true,
							animate: false,
							description: 'This is a Bootstrap Dialog',
							onshow: function(dialog) {
								dialog.getButton('button-c').disable();
							},
							buttons: [{
								label: '(A) Button A',
								hotkey: 65, // Keycode of keyup event of key 'A' is 65.
								action: function() {
									alert('Finally, you loved Button A.');
								}
							}, {
								label: '(B) Button B',
								hotkey: 66,
								action: function() {
									alert('Hello, this is Button B!');
								}
							}, {
								id: 'button-c',
								label: '(C) Button C',
								hotkey: 67,
								action: function(){
									alert('This is Button C but you won\'t see me dance.');
								}
							},{
								label: 'Close this dialog.',
								action: function(dialogRef){
									dialogRef.close();
								}
							},]
						});
					}
				},
			});
		}
	};
	return MODEL;
});