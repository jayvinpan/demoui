<ul id="appTab" class="nav nav-tabs">
   <li class="active">
      <a href="#home" data-toggle="tab"><i class="icon icon-heart"></i><span>Home</span><i class="tab_remove">×</i></a>
   </li>
</ul>
<div id="appTabContent" class="tab-content">
	<div class="tab-pane fade in active" id="home">
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">Demo</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<p>
					Hello world!!
				</p>
			</div>
			<div class="overlay">
				<i class="fa fa-refresh fa-spin"></i>
			</div>
			<!-- /.box-body -->
			<!--
			<div class="box-footer" >
				Footer
			</div>
			-->
		</div>
	</div>
</div>