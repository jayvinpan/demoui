(function ($) {
	var defaults = {     
			pages:{},
			sucess:function(data){
				console.log('default sucess !' + data);
			}
	};
	var methods = {
		init: function (options) {
			options = $.extend(defaults, options);
			var data = $(this).data('thinkSwitch');
			var pages = {};			
			if (!data) {
				console.log("no data and write to div data");
				this.data('thinkSwitch',options);
				data = $(this).data('thinkSwitch'); 
			} 
			pages = data.pages;
			$(pages).each(function(index,item) {		
				if( item.order == 0 || (item.order == undefined && index == 0)){
					console.log("[ page id  " + item.id + " show ]" ); 
					$("#"+item.id).show();
				} else {
					$("#"+item.id).hide();
				}
				if(item.init && typeof item.init != 'undefined' && item.init != undefined){
					item.init().apply(this, arguments);
				}
			});
//			if(options.sucess && typeof options.sucess != 'undefined' && options.sucess != undefined){
//				options.sucess();
//			}			
		},
		switchPage:function(id,data) {
			console.log("switchPage  " + id + "    "+  this.attr('class'));
			var pages = this.data('thinkSwitch').pages;	
			console.log("pages  " + "  " + pages.length);
			if (pages) {				
				$(pages).each(function(index,item){
					console.log("page id  " + item.id + "  " + index  + " order --->" +item.order);
					if( item.id == id){
						$("#"+item.id).fadeIn("slow");
						if(item.sucess && typeof item.sucess != 'undefined' && item.sucess != undefined){
							item.sucess(data);
						} else {
							console.log("no found callback functions");
						}
					} else {
						$("#"+item.id).hide();
					}					
				});
			}
		}
	};
	$.fn.extend({ 
		TinkerSwitch:function(method) {
			if (methods[method]) {
				return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
			} else if (typeof method === 'object' || !method) {
				return methods.init.apply(this, arguments);
			} else {
				$.error('Method ' + method + ' does not exist on jQuery.thinkSwitch');
			}
		}
    });
})(jQuery);
